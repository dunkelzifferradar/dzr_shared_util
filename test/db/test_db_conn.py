import unittest
from datetime import datetime
from sys import stderr

from dotenv import load_dotenv
from pandas import DataFrame

from dzr_shared_util.consts import ROOT_DIR

load_dotenv(ROOT_DIR + "/../.env")

from dzr_shared_util.db.db_conn import DBConnector

cut = DBConnector()
TEST_TABLE_NAME = 'TEST_PYTHON_DI_RKI'
TEST_COL_NAME = 'TEST_COL'
TEST_COL2_NAME = 'TEST_COL2'
TEST_COL3_NAME = 'TEST_COL3'
TEST_COL4_NAME = 'TEST_DATE_COL'
TEST_VIEW_NAME = 'TEST_PYTHON_DI_RKI_VIEW'
TEST_PROC_NAME = 'TEST_PROC_PY'


def delete_test_table():
    cut.engine.execute("DROP TABLE " + TEST_TABLE_NAME)


def delete_test_view():
    cut.engine.execute("DROP VIEW " + TEST_VIEW_NAME)


def delete_test_proc():
    cut.engine.execute("DROP PROCEDURE " + TEST_PROC_NAME)


def create_test_df(row_count, dt=datetime.now()):
    df = DataFrame()
    df[TEST_COL_NAME] = [r for r in range(0, row_count)]
    df[TEST_COL2_NAME] = 'a'
    df[TEST_COL3_NAME] = 'k'
    df[TEST_COL4_NAME] = dt
    return df


def create_test_table():
    sql_stmt = "CREATE TABLE " + TEST_TABLE_NAME + " (" + TEST_COL_NAME + " INTEGER, " + TEST_COL2_NAME + " VARCHAR(2), " + TEST_COL3_NAME + " CHAR(1), " + TEST_COL4_NAME + " TIMESTAMP)"
    run_sql_stmt(sql_stmt)


def run_sql_stmt(sql_stmt):
    try:
        cut.engine.execute(
            sql_stmt)
    except Exception as e:
        print(str(e), file=stderr)
        cut.close_connection()


def create_test_view():
    try:
        cut.engine.execute(
            "CREATE VIEW " + TEST_VIEW_NAME + " as select * from " + TEST_TABLE_NAME)
    except Exception as e:
        print(str(e), file=stderr)
        cut.close_connection()


def create_test_proc():
    proc_stmt = "CREATE PROCEDURE " + TEST_PROC_NAME + "(test_param integer) " \
                                                       "LANGUAGE SQL AS $$ " \
                                                       "SELECT *, test_param FROM " + TEST_TABLE_NAME + " LIMIT 1 $$;"
    run_sql_stmt(proc_stmt)


class TestDbConn(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        create_test_table()
        create_test_view()
        create_test_proc()

    def test_read_where_in(self):
        # first, empty table and write a bunch of data and check that it worked
        row_count_1 = 20
        df = create_test_df(row_count_1)
        cut.write_data(df, TEST_TABLE_NAME, clear_before_load=True)
        selected_rows = [5, 6, 7, 8]
        df_tbl_1 = cut.get_data(TEST_TABLE_NAME,
                                where=TEST_COL_NAME + " in (" + ",".join([str(x) for x in selected_rows]) + ")")
        self.assertEqual(set(df_tbl_1[TEST_COL_NAME].to_list()), set(selected_rows))

    def test_read_where_greater(self):
        # first, empty table and write a bunch of data and check that it worked
        row_count_1 = 20
        df = create_test_df(row_count_1)
        cut.write_data(df, TEST_TABLE_NAME, clear_before_load=True)
        row_start = 5
        row_end = 15
        selected_rows = range(row_start, row_end + 1)
        df_tbl_1 = cut.get_data(TEST_TABLE_NAME,
                                where=TEST_COL_NAME + " >= " + str(row_start) +
                                      " and " + TEST_COL_NAME + " <= " + str(row_end))
        self.assertEqual(set(df_tbl_1[TEST_COL_NAME].to_list()), set(selected_rows))

    def test_read_all_from_view(self):
        # first, empty table and write a bunch of data and check that it worked
        row_count_1 = 20
        df = create_test_df(row_count_1)
        cut.write_data(df, TEST_TABLE_NAME, clear_before_load=True)
        df_view = cut.get_data(TEST_VIEW_NAME)
        self.assertEqual(row_count_1, len(df_view.index))

    def test_read_selectively_from_view(self):
        # first, empty table and write a bunch of data and check that it worked
        row_count_1 = 20
        df = create_test_df(row_count_1)
        cut.write_data(df, TEST_TABLE_NAME, clear_before_load=True)
        df_view = cut.get_data(TEST_VIEW_NAME,
                               where=TEST_COL_NAME + " < " + str(row_count_1 / 2))
        self.assertEqual(row_count_1 / 2, len(df_view.index))

    def test_append_data(self):
        # first, empty table and write a bunch of data and check that it worked
        row_count_1 = 20000
        df = create_test_df(row_count_1)
        cut.write_data(df, TEST_TABLE_NAME, clear_before_load=True)
        df_tbl_1 = cut.get_data(TEST_TABLE_NAME)
        self.assertEqual(row_count_1, len(df_tbl_1.index))
        # second, append a bit of data and check that it worked
        row_count_2 = 5
        df = create_test_df(row_count_2)
        cut.write_data(df, TEST_TABLE_NAME, clear_before_load=False)
        df_tbl_after = cut.get_data(TEST_TABLE_NAME)
        self.assertEqual(row_count_2 + row_count_1, len(df_tbl_after.index))

    def test_clear_and_insert_data(self):
        # first, empty table and write a bunch of data and check that it worked
        row_count_1 = 100
        df = create_test_df(row_count_1)
        cut.write_data(df, TEST_TABLE_NAME, clear_before_load=True)
        df_tbl_1 = cut.get_data(TEST_TABLE_NAME)
        self.assertEqual(row_count_1, len(df_tbl_1.index))
        # second, append a bit of data and check that it worked
        row_count_2 = 5
        df = create_test_df(row_count_2)
        cut.write_data(df, TEST_TABLE_NAME, clear_before_load=True)
        df_tbl_after = cut.get_data(TEST_TABLE_NAME)
        self.assertEqual(row_count_2, len(df_tbl_after.index))

    def test_update_table(self):
        # first, empty table and write some data
        row_count_1 = 20
        df = create_test_df(row_count_1)
        cut.write_data(df, TEST_TABLE_NAME, clear_before_load=True)
        df_tbl_1 = cut.get_data(TEST_TABLE_NAME)
        self.assertEqual(row_count_1, len(df_tbl_1.index))
        # second, update data and check it worked
        row_count_2 = 10
        df2 = create_test_df(row_count_2)
        df2[TEST_COL3_NAME] = 'l'
        cut.update_data(df2, TEST_TABLE_NAME, update_keys=[TEST_COL_NAME, TEST_COL2_NAME],
                        update_vals=[TEST_COL2_NAME, TEST_COL3_NAME])
        df_tbl_after_update = cut.get_data(TEST_TABLE_NAME)
        self.assertEqual(row_count_1, len(df_tbl_after_update.index))
        self.assertEqual(list(df_tbl_after_update[TEST_COL3_NAME].unique()), ['k', 'l'])

    def test_delete_batch_entries(self):
        # first, empty table and write some data
        row_count_1 = 20
        curr_time = datetime.now()
        df = create_test_df(row_count_1, dt=curr_time)
        cut.write_data(df, TEST_TABLE_NAME, clear_before_load=True)
        df_tbl_1 = cut.get_data(TEST_TABLE_NAME)
        self.assertEqual(row_count_1, len(df_tbl_1.index))
        # second, batch-delete entries and check
        row_count_2 = 10
        df_deletes = create_test_df(row_count_2, dt=curr_time)
        cut.delete_data(TEST_TABLE_NAME, where=df_deletes[[TEST_COL_NAME, TEST_COL2_NAME, TEST_COL4_NAME]])
        df_res = cut.get_data(TEST_TABLE_NAME)
        self.assertEqual(len(df_res.index), row_count_1 - row_count_2)

    def test_rollback(self):
        # first, empty table and write some data (WITHOUT COMMIT)
        row_count_1 = 20
        curr_time = datetime.now()
        df = create_test_df(row_count_1, dt=curr_time)
        cut.write_data(df, TEST_TABLE_NAME, clear_before_load=True, commit=False)
        df_tbl_1 = cut.get_data(TEST_TABLE_NAME)
        self.assertEqual(row_count_1, len(df_tbl_1.index))
        # second, roll back, then check that table is still empty, inserts have not been committed!
        cut.rollback()
        df_tbl_after = cut.get_data(TEST_TABLE_NAME)
        self.assertEqual(len(df_tbl_after.index), 0)

    def test_call_procedure(self):
        # call procedure
        cut.call_procedure("TEST_PROC_PY", [1])
        # no return to test here - procedures don't return anything.

    @classmethod
    def tearDownClass(cls):
        try:
            delete_test_view()
            delete_test_table()
            delete_test_proc()
        finally:
            cut.close_connection()


if __name__ == '__main__':
    unittest.main()
