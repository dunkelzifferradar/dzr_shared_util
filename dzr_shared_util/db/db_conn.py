import io
import logging
import os
from typing import Union

import pandas as pd
from pandas import DataFrame
from pandas.errors import EmptyDataError
from psycopg2.extensions import cursor
from psycopg2.extras import execute_batch
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DBEngineException(Exception):

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class DBConnectionException(Exception):

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class DBQueryException(Exception):

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class DBDataCopyException(Exception):

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


def upper_case_columns(df):
    if df is None:
        raise ValueError('Returned data is None')
    if df.columns is None:
        raise ValueError('Columns of returned data is None')
    df.columns = [col_name.upper() for col_name in df.columns]
    return df


class DBConnector:
    engine: Engine

    def __init__(self, db_name=os.environ.get('DB_NAME'), db_user=os.environ.get('DB_USER'),
                 db_password=os.environ.get('DB_PASSWORD'), db_host=os.environ.get('DB_HOST'),
                 db_port=os.environ.get('DB_PORT'), db_ssl_cert=os.environ.get('DB_SSL_CERT'),
                 db_ssl_key=os.environ.get('DB_SSL_KEY')):
        """
        Class for connecting to the DunkelzifferRadar database.
        :param db_name: string: name of the database
        :param db_host: string: database host
        :param db_port: string: database port
        :param db_user: string: username in the database
        :param db_password: string: password corresponding to the username
        :param db_ssl_cert: string: path to SSL certificate
        :param db_ssl_key: string: path to SSL key
        """
        # todo: switch to ssl when moving to cloud
        # This code duplication from the default values of the function params is currently necessary...apparently,
        # the environment variables set in the importing projects are not available yet when calling the __init__
        # method, but are here:
        if db_name is None:
            db_name = os.environ.get('DB_NAME')
        if db_user is None:
            db_user = os.environ.get('DB_USER')
        if db_password is None:
            db_password = os.environ.get('DB_PASSWORD')
        if db_host is None:
            db_host = os.environ.get('DB_HOST')
        if db_port is None:
            db_port = os.environ.get('DB_PORT')
        if db_ssl_cert is None:
            db_ssl_cert = os.environ.get('DB_SSL_CERT')
        if db_ssl_key is None:
            db_ssl_key = os.environ.get('DB_SSL_KEY')
        if any(not isinstance(s, str) for s in [db_user, db_password, db_host, db_port, db_name]):
            raise ValueError(
                "DB parameters user, password, host, port, db name must not be null. "
                "Seems like the environment variables were not set")
        postgres_connection_string = "postgresql+psycopg2://" + db_user + ":" + db_password + "@" + db_host + ":" \
                                     + db_port + "/" + db_name
        try:
            self.engine = create_engine(postgres_connection_string)
        except Exception as e:
            raise DBEngineException(e)
        try:
            self.conn = self.engine.raw_connection()
        except Exception as e:
            raise DBConnectionException(e)

    def close_connection(self):
        """
        Close the connection to the database.

        :return: None
        """
        if self.conn is not None:
            self.conn.close()

    def write_data(self, df: pd.DataFrame, target_table_name, clear_before_load=False, commit=True):
        """
        All columns of the dataframe (except Unnamed/index cols) must exist (identically, but in uppercase)
        in the target table. Before insertion, column names of the dataframe are uppercased.
        :param df: data to insert
        :param target_table_name: table to write into
        :param clear_before_load: if set to True (not default), target table is emptied before data is loaded.
        :param commit: whether to commit the transaction.
        :return:
        """
        if target_table_name is None:
            raise ValueError("Table name must not be None")
        if df is None or df.empty:
            logger.debug("Dataframe to be written to DB table " + target_table_name + " is None. Doing nothing.")
            return
        df = upper_case_columns(df)
        # make sure that target table has same structure as dataframe to be inserted
        df_target_empty: DataFrame = self.init_df_schema(target_table_name)
        assert set(df_target_empty.columns) == set(df.columns), \
            "Target table columns are different from those to be inserted. Target: " + str(
                df_target_empty.columns) + " vs. inserted: " + str(df.columns)
        # clear table if requested, but put commit on False, so that the "clear table" and the "insert data" is
        # in the same transaction
        if clear_before_load:
            self.delete_data(target_table_name, commit=False)
        cur = self.__get_cursor()
        try:
            output = io.StringIO()
            df.to_csv(output, sep='\t', header=False, index=False)
            output.seek(0)
            contents = output.getvalue()
            cur.copy_from(output, target_table_name, null='', columns=df.columns)
            if commit:
                self.commit()
        except Exception as e:
            self.rollback()
            raise DBDataCopyException("Copying dataframe data over to database failed.") from e
        finally:
            cur.close()

    def __get_cursor(self) -> cursor:
        if self.conn is None:
            raise AttributeError("Database connection must not be None")
        try:
            cur = self.conn.cursor()
        except Exception as e:
            raise DBConnectionException(e)
        return cur

    def delete_data(self, target_table_name, where: Union[str, DataFrame] = "", commit=True):
        """
        Delete data from a table. If no "where" is given, all data is deleted. If where is given as a string, a single
        delete sql statement is executed. If it is given as a dataframe, a batch delete is executed for better
        performance.
        :param target_table_name: the table to delete data in
        :param where: can be a sql where string (without the keyword "WHERE") or a dataframe, which contains exactly
        all columns that are used to build a where statement. For each row of the dataframe, a delete statement is
        built.
        :return:
        """
        if target_table_name is None:
            raise ValueError("Table name must not be None")
        query_string = "DELETE FROM " + target_table_name
        if where is None or isinstance(where, str):
            if len(where) > 0:
                query_string += " WHERE " + where
            try:
                self.engine.execute(query_string)
            except Exception as e:
                raise DBQueryException("Query " + query_string + " failed.") from e
        elif isinstance(where, DataFrame):
            df_where: DataFrame = where
            assert not df_where.isnull().values.any(), "There are N/A values in the dataframe! Remove them first"
            cur = self.__get_cursor()
            try:
                cols = [df_where[col].tolist() for col in df_where.columns]
                entry_key_vals = list(zip(*cols))
                query = "DELETE FROM " + target_table_name + " WHERE "
                where_stmt = " AND ".join([col + " = %s" for col in df_where.columns])
                query = query + where_stmt
                execute_batch(cur, query, entry_key_vals)
                if commit:
                    self.commit()
            except Exception as e:
                self.rollback()
                raise DBQueryException("Query " + query_string + " failed.") from e
            finally:
                cur.close()

    def get_data(self, tbl_name, where="") -> DataFrame:
        """
        Retrieves data for the given table or view.
        :param tbl_name:
        :param where: where clause (without the word "WHERE"), sql as string, e.g. "name = 'bla'"
        :return: the data as a DataFrame
        """
        if tbl_name is None:
            raise ValueError("Table name must not be None")
        if where is None or len(where) == 0:
            try:
                df = self._get_full_table_data(tbl_name)
            except DBDataCopyException as dbce:
                logger.debug("Using copy_to to get full table/view content failed. Probably because " + tbl_name +
                             " is a view. Retrying with SELECT query...")
                df = self._get_data_select(tbl_name)
        else:
            df = self._get_data_select(tbl_name, where)
        return upper_case_columns(df)

    def _get_data_select(self, tbl_name, where=None):
        query_string = "SELECT * FROM " + tbl_name
        if where is not None and where != "":
            query_string += " WHERE " + where
        try:
            df = pd.read_sql(query_string, con=self.engine)
        except Exception as e:
            self.rollback()
            raise DBQueryException("Query '" + query_string + "' failed.") from e
        return df

    def _get_full_table_data(self, tbl_name):
        """
        Uses the copy mechanism for postgres which is supposed to be much faster.
        Only works for tables, not for views.
        :param tbl_name:
        :return:
        """
        df_tbl_cols = self.init_df_schema(tbl_name)
        cur = self.conn.cursor()
        try:
            output = io.StringIO()
            cur.copy_to(output, tbl_name, null="", sep='\t')
            output.seek(0)
            df = pd.read_csv(output, sep='\t', header=None)
            df.columns = df_tbl_cols.columns
        except EmptyDataError as ede:
            return df_tbl_cols
        except Exception as e:
            self.rollback()
            raise DBDataCopyException("Copying table data over from database failed.") from e
        finally:
            cur.close()
        return df

    def init_df_schema(self, tbl_name):
        if tbl_name is None:
            raise ValueError("Table name must not be None")
        query_string = "SELECT * FROM " + tbl_name + " LIMIT 0"
        try:
            df_tbl_cols = pd.read_sql(query_string, con=self.engine)
        except Exception as e:
            raise DBQueryException("Query " + query_string + " failed.") from e
        return upper_case_columns(df_tbl_cols)

    def update_data(self, df: DataFrame, target_table_name: str, update_keys: list, update_vals=None, commit=True):
        """
        Update the columns of all entries in the provided table for which the given key columns match the provided
        dataframe's column values (with the same column name).
        :param df:
        :param target_table_name:
        :param update_keys:
        :param update_vals:
        :return:
        """
        if df is None or df.empty:
            logger.debug("Dataframe for updating DB table " + target_table_name + " is None. Doing nothing.")
            return
        assert not df.isnull().values.any(), "There are N/A values in the dataframe! Remove them first"
        assert update_keys is not None and len(update_keys) > 0
        if update_vals is None:
            update_val_cols = [col for col in df.columns if col not in update_keys]
        else:
            assert isinstance(update_vals, list)
            update_val_cols = update_vals
        cur = self.__get_cursor()
        try:
            cols = [df[col].tolist() for col in update_val_cols + update_keys]
            entry_key_vals = list(zip(*cols))
            query = "UPDATE " + target_table_name + " SET "
            set_stmt = ", ".join([col + " = %s" for col in update_val_cols])
            where_stmt = " AND ".join([col + " = %s" for col in update_keys])
            query = query + set_stmt + " WHERE " + where_stmt
            execute_batch(cur, query, entry_key_vals)
            if commit:
                self.commit()
        except Exception as e:
            raise e
        finally:
            cur.close()

    def call_procedure(self, proc_name: str, args: list, commit=True):
        """
        Calls a database procedure.
        :param proc_name: Procedure name
        :param args: list of arguments. Must be in order as the procedure expects it
        :return: None
        """
        query_string = "CALL " + proc_name + "(" + ", ".join([str(arg) for arg in args]) + ")"
        try:
            self.engine.execute(query_string)
            if commit:
                self.commit()
        except Exception as e:
            raise DBQueryException("Query " + query_string + " failed.") from e

    def commit(self):
        if self.conn is not None:
            self.conn.commit()
        else:
            raise ValueError("Connection has not been established, hence it cannot be committed")

    def rollback(self):
        self.conn.rollback()
