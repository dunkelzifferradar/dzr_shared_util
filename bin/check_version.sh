#!/usr/bin/env bash

pyproject_version() {
    # get version defined in pyproject.toml
    while IFS="" read -r line || [[ -n "$line" ]]
    do
        key="$(cut -d'=' -f1 <<<"$line" | tr -d '[:space:]')"
        value="$(cut -d'=' -f2 <<<"$line" | tr -d '[:space:]\"')"
        if [[ "$key" == "version" ]]; then
            echo "$value"
        fi
    done < ./pyproject.toml
}

check_version() {
    # make sure $CI_COMMIT_TAG and pyproject_version match
    pp_version="$(pyproject_version)"
    if [[ ! "$pp_version" =~ "$CI_COMMIT_TAG" ]]; then
        echo "Version mismatch between pyproject.toml ($pp_version) and tag ($CI_COMMIT_TAG)"
        exit 1
    fi
}

check_version